(defproject ofx2csv "0.1.0-SNAPSHOT"
  :license {:name "The Unlicense"
            :url "http://unlicense.org/"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [ofx-clj "0.1"]
                 [org.clojure/tools.cli "0.4.1"]]
  :main ofx2csv.cli)
