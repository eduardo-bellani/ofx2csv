(ns ofx2csv.consolidated-transactions
  (:require [ofx-clj.core :as ofx]
            [clojure.set  :as cset]))

(def ^:private base-currency :base)

(defn- transactions->credit-card-transactions
  "Consolidates the credit card transactions into a single map."
  [transactions]
  (ofx/find-all-transactions transactions "creditcard"))

(defn- group-by-currency-exchange-rate
  "Groups the data by currency and their exchange rate. In the return map, replace
  nil for the base currency symbol as to make it more accessible."
  [transactions]
  (cset/rename-keys (group-by :currency transactions)
                           {nil base-currency}))

(defn- fx->base
  "Given a map of the fx transactions, map them to the base transaction. This is
  necessary for consolidation."
  [fx-transactions]
  (flatten (for [fx-xr (keys fx-transactions)]
             ;; bigdecimal is used by ofx, so this
             ;; assures that everything stays the
             ;; same format.
             (let [xr (BigDecimal. (str (:exchange-rate fx-xr)))]
               (map #(update % :amount (fn [amount]
                                         (* amount xr)))
                    (get fx-transactions fx-xr))))))

(defn- consolidate-transactions
  "From the transactions grouped by currency and exchange rate this returns a
  single sequence of transactions, applying the exchange rate. The point here is
  to consolidate the data in order to be able to export it to a CSV."
  [grouped-transactions]
  (-> grouped-transactions
      (dissoc base-currency)
      fx->base
      (concat (get grouped-transactions base-currency))))

(defn transactions->consolidated-transactions
  "From the OFX transactions, return the consolidated transactions that are
  suitable to send to the output processor. For now, only credit card
  transactions are supported."
  [transactions]
  (-> transactions
      transactions->credit-card-transactions
      group-by-currency-exchange-rate
      consolidate-transactions))
