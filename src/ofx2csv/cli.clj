(ns ofx2csv.cli
  (:require [ofx2csv.core                      :as core]
            [clojure.java.io                   :as io]
            [clojure.tools.cli                 :as cli]
            [clojure.string                    :as string])
  (:gen-class))

(defn- is-existing-file?
  "Used for validation of the inputs."
  [filename]
  (.exists (io/file filename)))

(def ^:private cli-options
  [["-o" "--output CSV-FILE-PATH" "Output file path to write the CSV data. Will overwrite existing files." :default "output.csv"]
   ["-h" "--help"]])

(defn- usage [options-summary]
  (->> ["ofx2csv is a CLI to help you turn your OFX files into CSV files. This
may be helpful for your ledger pipeline, specially if combined with tools such
as banks2ledger(https://github.com/tomszilagyi/banks2ledger)."
        ""
        "Usage: ofx2csv [options] ofx-file"
        ""
        "Options:"
        options-summary]
       (string/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn- validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (:help options) ; help => exit OK with usage summary
      {:exit-message (usage summary) :ok? true}
      errors ; errors => exit with description of errors
      {:exit-message (error-msg errors)}
      (and (= 1 (count arguments)) (is-existing-file? (first arguments)))
      {:input (first arguments) :options options}
      :else ; failed custom validation => exit with usage summary
      {:exit-message (usage summary)})))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [input options exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (do (with-open [w (io/writer (:output options))]
            (core/ofx-file-path->csv-file input w))
          (exit 0 "ofx2csv finished")))))
