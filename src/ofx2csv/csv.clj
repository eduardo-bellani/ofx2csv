(ns ofx2csv.csv
  (:require [clojure.data.csv :as csv]))

(def ^:private header ["date" "description" "amount"])

(def ^:private one-day-unix-time (* 1000 1 3600 24))

(defn- unix-time->java-date
  "Return a Java Date object from a Unix time representation expressed in whole
  seconds. Adds a day to the result, since the OFX resolution is day, and this
  unix time was resulting in an offset error of a day."
  [unix-time]
  (java.util.Date. (+ unix-time one-day-unix-time)))

(defn- java-date->csv-date
  "Used to produce dates in the format suitable for exportation to the CSV"
  [java-date]
  (.format (java.text.SimpleDateFormat. "yyyy/MM/dd") java-date))

(defn consolidated-transactions->output-transactions
  "Maps the consolidated transactions to a format suitable to writing into a CSV
  file."
  [consolidated-transactions]
  (map (fn [transaction]
         [(-> transaction
              :date-posted
              unix-time->java-date
              java-date->csv-date)
          (:memo transaction)
          (:amount transaction)])
       consolidated-transactions))

(defn output-transactions->csv
  "Writes the output tranactions as a CSV"
  [output-transactions writer]
  (csv/write-csv writer
                 (conj output-transactions header)))
