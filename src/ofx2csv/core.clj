(ns ofx2csv.core
  (:require [ofx-clj.core                      :as ofx]
            [ofx2csv.csv                       :as csv]
            [ofx2csv.consolidated-transactions :as ct]
            [clojure.java.io                   :as io]))

(defn ofx-file-path->csv-file
  "The function that actually ties everything togueter."
  [ofx-file-path writer]
  (-> ofx-file-path
      ofx/parse
      ct/transactions->consolidated-transactions
      csv/consolidated-transactions->output-transactions
      (csv/output-transactions->csv writer)))
