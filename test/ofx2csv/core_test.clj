(ns ofx2csv.core-test
  (:require [clojure.test :refer :all]
            [ofx2csv.core :refer :all]))

(deftest reading-and-writing
  (let [string-writer (java.io.StringWriter.)
        test-return
"date,description,amount
2018/08/03,Transaction in USD,-61.497044
2018/08/17,Transaction in EUR,-930.695844
2018/08/10,Expenditure in base currency,14.89
"]
    (ofx-file-path->csv-file "resources/test.ofx" string-writer)
    (is (= (str string-writer) test-return))))
